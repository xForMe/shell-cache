package com.crazy.shell.cache.annotation;

import java.lang.annotation.*;

/**
 * Created by creazier.huang on 16/4/29.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface CacheDelete {


    CacheDeleteKey[] value();
}
