package com.crazy.shell.cache.annotation;

import com.crazy.shell.cache.type.CacheOpType;

import java.lang.annotation.*;

/**
 * Created by creazier.huang on 16/4/29.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface CacheDeleteKey {

    /**
     * 缓存条件
     * @return
     */
    String condition() default "";

    /**
     * 删除缓存的key
     * @return
     */
    String key();

    /**
     * hash表中字段
     * @return
     */
    String hfield() default "";
}
