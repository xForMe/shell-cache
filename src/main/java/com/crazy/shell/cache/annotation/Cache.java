package com.crazy.shell.cache.annotation;

import com.crazy.shell.cache.type.CacheOpType;

import java.lang.annotation.*;
import java.util.List;

/**
 * Created by creazier.huang on 16/4/29.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface Cache {
    /**
     * 缓存过期时间,单位 : 秒 ,0:永久
     * @return
     */
    int expire();

    /**
     * 自定义缓存key
     * @return
     */
    String key();

    /**
     * 设置hash表字段,如果设置此项用hash表进行存储
     * @return
     */
    String hfield() default "";

    /**
     * 是否自动加载缓存,缓存时间大于120s有效
     * @return
     */
    boolean autoload() default false;

    /**
     * 自动缓存条件
     * @return
     */
    String autoloadCondition() default "";

    /**
     * 当autoLoad=true 缓存数据在requestTimeOut 秒内没有使用将不再自动加载数据
     * requestTimeOut=0会一直自动加载
     * @return
     */
    long requestTimeout() default 36000L;

    /**
     * 缓存条件
     * @return
     */
    String condition() default "";
    /**
     * 缓存的操作类型：默认是READ_WRITE，先缓存取数据，如果没有数据则从DAO中获取并写入缓存；如果是WRITE则从DAO取完数据后，写入缓存
     * @return CacheOpType
     */
    CacheOpType opType() default CacheOpType.READ_WRITE;
    /**
     * 并发等待时间(毫秒),等待正在DAO中加载数据的线程返回的等待时间。
     * @return 时间
     */
    int waitTimeOut() default 500;
    /**
     * 缓存扩展
     */
    ExCache[] exCache() default @ExCache(expire=-1,key="");
}
