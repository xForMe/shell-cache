package com.crazy.shell.cache.annotation;

import java.lang.annotation.*;

/**
 * shell-cache
 * 控制每个类的cache
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/6.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
@Documented
public @interface CacheConfig {
    /**
     * 是否启用缓存
     * @return
     */
    boolean enable() default true;

    /**
     * 缓存类型
     * @return
     */
    Class clazz();

    /**
     * 关键字前缀
     * @return
     */
    String preKey();
}
