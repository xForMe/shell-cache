package com.crazy.shell.cache;

import com.crazy.shell.cache.annotation.Cache;
import com.crazy.shell.cache.to.AutoLoadTO;
import com.crazy.shell.cache.to.CacheKeyTO;
import com.crazy.shell.cache.to.CacheWrapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Created by creazier.huang on 16/5/1.
 */
public interface ICacheManager {

    /**
     * 往缓存里面写数据
     * @param cacheKeyTO
     * @param result
     */
    void setCache(CacheKeyTO cacheKeyTO, CacheWrapper result);



    /**
     * 根据缓存key获取缓存内容
     * @param key
     * @return
     */
    CacheWrapper getCache(CacheKeyTO key);

    /**
     * 删除缓存
     * @param key 缓存key
     */
    void deleteCache(CacheKeyTO key);

    /**
     * 设置自动加载缓存
     * @param key
     * @param pjp
     */
    void setAutoCache(CacheKeyTO key,ProceedingJoinPoint pjp);


    /**
     * 获取自动加载处理器
     * @return  自动加载处理器
     */
    AutoLoadHandle getAutoLoadHandler();

    /**
     * 关闭线程
     */
    void destory();

    /**
     * 加载数据
     * @param pjp
     * @param autoLoadTO
     * @param key
     * @param cache
     * @return
     */
    Object loadData(ProceedingJoinPoint pjp, AutoLoadTO autoLoadTO, CacheKeyTO key, Cache cache);

}
