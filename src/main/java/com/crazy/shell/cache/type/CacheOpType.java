package com.crazy.shell.cache.type;

/**
 * 缓存操作类型
 * Created by creazier.huang on 16/4/29.
 */
public enum CacheOpType {
    /**
     * 读写缓存操作
     */
    READ_WRITE,
    /**
     * 写缓存
     */
    WRITE;
}
