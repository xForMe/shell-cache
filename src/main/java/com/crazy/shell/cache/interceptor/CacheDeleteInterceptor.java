package com.crazy.shell.cache.interceptor;

import com.crazy.shell.cache.annotation.CacheConfig;
import com.crazy.shell.cache.annotation.CacheDelete;
import com.crazy.shell.cache.annotation.CacheDeleteKey;
import com.crazy.shell.cache.cacheManager.RedisManager;
import com.crazy.shell.cache.to.CacheKeyTO;
import com.crazy.shell.cache.util.RefectionUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.lang.reflect.Method;

/**
 * shell-cache
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/17.
 */
public class CacheDeleteInterceptor {
    private Logger logger = LoggerFactory.getLogger(CacheDeleteInterceptor.class);

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Resource
    private RedisManager redisManager;

    public Object doArround(ProceedingJoinPoint jp) throws Throwable {
        Object o = null;
        Object tar = jp.getTarget();

        CacheConfig cacheConfig = tar.getClass().getAnnotation(CacheConfig.class);
        if(cacheConfig==null || cacheConfig.enable() == false){
            o = jp.proceed();
            return  o;
        }

        Signature signature = jp.getSignature();

        Method targetMethod = RefectionUtil.getAccessibleMethodByName(tar,signature.getName());
        CacheDeleteKey cacheDel = targetMethod.getAnnotation(CacheDeleteKey.class);
        if(cacheDel == null){
            return jp.proceed();
        }
        CacheKeyTO key = new CacheKeyTO(cacheConfig.preKey(),cacheDel.key());
        redisManager.deleteCache(key);
        o = jp.proceed();
        return  o;

    }


}
