package com.crazy.shell.cache.interceptor;

import com.alibaba.fastjson.JSON;
import com.crazy.shell.cache.annotation.Cache;
import com.crazy.shell.cache.annotation.CacheConfig;
import com.crazy.shell.cache.cacheManager.RedisManager;
import com.crazy.shell.cache.to.CacheKeyTO;
import com.crazy.shell.cache.to.CacheWrapper;
import com.crazy.shell.cache.type.CacheOpType;
import com.crazy.shell.cache.util.RefectionUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * redis cache 缓存拦截器
 * Created by creazier.huang on 16/4/29.
 */
public class CacheInterceptor {
    private Logger logger = LoggerFactory.getLogger(CacheInterceptor.class);

    @Resource
    private RedisManager redisManager;



    public Object doArround(ProceedingJoinPoint jp) throws Throwable {

        Object o = null;
        Object tar = jp.getTarget();


        Signature signature = jp.getSignature();

        Method targetMethod = RefectionUtil.getAccessibleMethodByName(tar,signature.getName());

        Cache cacheAnn =targetMethod.getAnnotation(Cache.class);
        CacheConfig cacheConfig = tar.getClass().getAnnotation(CacheConfig.class);
        if(cacheConfig==null || cacheConfig.enable() == false){
            o = jp.proceed();
            return  o;
        }

        CacheKeyTO key = new CacheKeyTO(cacheConfig.preKey(),cacheAnn.key());
        CacheWrapper result = new CacheWrapper(cacheAnn.expire());

        if(cacheAnn.opType().equals(CacheOpType.READ_WRITE)){
            if(redisManager.getCache(key) == null){
                o = jp.proceed();
                result.setCacheObject(o);
                redisManager.setCache(key,result);
                return result;
            }else {
                result = redisManager.getCache(key);
                o = result.getCacheObject();
            }
        }

        if(cacheAnn.opType().equals(CacheOpType.WRITE)){
            o = jp.proceed();
            result.setCacheObject(o);
            redisManager.setCache(key,result);
        }

        if(cacheAnn.autoload()){
            redisManager.setAutoCache(key,jp);
        }
        return  o;
    }

}
