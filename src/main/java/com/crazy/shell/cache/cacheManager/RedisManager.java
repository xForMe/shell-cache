package com.crazy.shell.cache.cacheManager;

import com.alibaba.fastjson.JSON;
import com.crazy.shell.cache.AutoLoadHandle;
import com.crazy.shell.cache.ICacheManager;
import com.crazy.shell.cache.annotation.Cache;
import com.crazy.shell.cache.constants.CacheKey;
import com.crazy.shell.cache.to.AutoLoadTO;
import com.crazy.shell.cache.to.CacheKeyTO;
import com.crazy.shell.cache.to.CacheWrapper;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * shell-cache
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/9.
 */
@Service
public class RedisManager implements ICacheManager {

    private static Logger logger = LoggerFactory.getLogger(RedisManager.class);


    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Resource(name="redisTemplate")
    private ValueOperations valueOps;

    @Resource(name="redisTemplate")
    private HashOperations<String,String,Object> hashOps;


    public void setCache(CacheKeyTO cacheKeyTO, CacheWrapper resultWrap) {
        String cacheKey = cacheKeyTO.getCacheKey();
        resultWrap.setLastLoadTime(System.currentTimeMillis());
        valueOps.set(cacheKey,resultWrap);

    }

    public CacheWrapper getCache(CacheKeyTO key) {
        String cacheKey = key.getCacheKey();
        CacheWrapper result = JSON.toJavaObject((JSON) valueOps.get(cacheKey),CacheWrapper.class);
        return result;
    }

    public void deleteCache(CacheKeyTO key) {
        String cacheKey = key.getCacheKey();
        redisTemplate.delete(cacheKey);

    }

    @Override
    public void setAutoCache(CacheKeyTO key, ProceedingJoinPoint pjp) {
        hashOps.put(CacheKey.AUTO_LOAD_CACHE,key.getFullKey(),pjp);
    }


    /**
     *
     * @return
     */
    public AutoLoadHandle getAutoLoadHandler() {
        return null;
    }

    public void destory() {

    }

    public Object loadData(ProceedingJoinPoint pjp, AutoLoadTO autoLoadTO, CacheKeyTO key, Cache cache) {
        return null;
    }
}
