package com.crazy.shell.cache.to;

import com.crazy.shell.cache.annotation.Cache;
import com.crazy.shell.cache.type.AutoLoadQueueSortType;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;

/**
 * shell-cache
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/9.
 */
public class AutoLoadConfig {

    private Integer threadCnt = 50;

    private int maxElement = 20000;

    private boolean printSlowLog = true;

    private int slowLoadTime = 500;


    private AutoLoadQueueSortType sortType = AutoLoadQueueSortType.NONE;

    private boolean checkFromCacheBeforeLoad = false;

    private int autoLoadPeriod = 50;

    public Integer getThreadCnt() {
        return threadCnt;
    }

    public void setThreadCnt(Integer threadCnt) {
        if(threadCnt <= 0){
            return;
        }
        this.threadCnt = threadCnt;
    }

    public int getMaxElement() {
        return maxElement;
    }

    public void setMaxElement(int maxElement) {
        if(maxElement <= 0){
            return;
        }
        this.maxElement = maxElement;
    }


    public boolean isPrintSlowLog() {
        return printSlowLog;
    }

    public void setPrintSlowLog(boolean printSlowLog) {
        this.printSlowLog = printSlowLog;
    }

    public int getSlowLoadTime() {
        return slowLoadTime;
    }

    public void setSlowLoadTime(int slowLoadTime) {
        if(slowLoadTime <0){
            return;
        }
        this.slowLoadTime = slowLoadTime;
    }

    public AutoLoadQueueSortType getSortType() {
        return sortType;
    }

    public void setSortType(Integer sortType) {
        this.sortType = AutoLoadQueueSortType.getById(sortType);
    }

    public boolean isCheckFromCacheBeforeLoad() {
        return checkFromCacheBeforeLoad;
    }

    public void setCheckFromCacheBeforeLoad(boolean checkFromCacheBeforeLoad) {
        this.checkFromCacheBeforeLoad = checkFromCacheBeforeLoad;
    }

    public int getAutoLoadPeriod() {
        return autoLoadPeriod;
    }

    public void setAutoLoadPeriod(int autoLoadPeriod) {
        if(autoLoadPeriod < 5){
            return;
        }
        this.autoLoadPeriod = autoLoadPeriod;
    }


//    public void setFunctions(Map<String,String> funcs){
//        if(null == funcs || funcs.isEmpty())
//            return;
//
//        Iterator<Map.Entry<String,String>> it = funcs.entrySet().iterator();
//        while (it.hasNext()){
//            Map.Entry<String,String> entry = it.next();
//
//            try {
//                String name = entry.getKey();
//                Class cls = Class.forName(entry.getValue());
//                Method method = cls.getDeclaredMethod(name,new Class[]{Object.class})
//            } catch (Exception e) {
//
//                e.printStackTrace();
//            }
//        }
//    }
}
