package com.crazy.shell.cache.to;

import java.io.Serializable;

/**
 * shell-cache
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/2.
 */
public class CacheWrapper implements Serializable{
    private static final long serialVersionUID=1L;

    /**
     * 缓存数据
     */
    private  Object cacheObject;
    /**
     * 最后加载数据时间
     */
    private long lastLoadTime;
    /**
     * 缓存时长
     */
    private int expire;

    public CacheWrapper() {
        this.lastLoadTime=System.currentTimeMillis();
    }

    public CacheWrapper(int expire) {
        this.expire = expire;
        this.lastLoadTime=System.currentTimeMillis();
    }

    public CacheWrapper(Object cacheObject, int expire) {
        this.cacheObject=cacheObject;
        this.lastLoadTime=System.currentTimeMillis();
        this.expire=expire;
    }


    public Object getCacheObject() {
        return cacheObject;
    }

    public void setCacheObject(Object cacheObject) {
        this.cacheObject = cacheObject;
    }

    public long getLastLoadTime() {
        return lastLoadTime;
    }

    public void setLastLoadTime(Long lastLoadTime) {
        this.lastLoadTime = lastLoadTime;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }
}
