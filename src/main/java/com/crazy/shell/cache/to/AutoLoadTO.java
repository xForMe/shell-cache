package com.crazy.shell.cache.to;

import com.crazy.shell.cache.annotation.Cache;
import org.aspectj.lang.ProceedingJoinPoint;

import java.io.Serializable;

/**
 * shell-cache
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/2.
 */
public class AutoLoadTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Object[] args;
    /**
     * 缓存注解
     */
    private Cache cache;
    /**
     * 缓存key
     */
    private CacheKeyTO cacheKeyTO;
    /**
     * 上次从dao中加载的时间
     */
    private long lastLoadTime = 0L;

    /**
     * 上次请求数据时间
     */
    private long lastRequestTime = 0L;

    /**
     * 第一次请求数据时间
     */
    private long firstRequestTime = 0L;
    /**
     * 请求次数
     */
    private long requestTimes = 0L;

    private volatile boolean loading = false;

    private String cacheClass;

    private String cacheMethod;

    /**
     * 加载次数
     */
    private long loadCnt = 0L;

    /**
     * 从DAO中加载数据，使用时间的总和
     */
    private long useTotalTime = 0L;

    private ProceedingJoinPoint pjp;

    /**
     * 记录用时
     *
     * @param useTime
     *            用时
     */
    public void addUseTotalTime(long useTime){
        synchronized (this){
            this.loadCnt ++;
            this.useTotalTime = this.useTotalTime+useTime;
        }
    }

    /**
     * 平均用时
     *
     * @return long 用时
     */
    public long getAverageUseTime() {
        if (loadCnt == 0) {
            return 0;
        }
        return this.useTotalTime / this.loadCnt;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Cache getCache() {
        return cache;
    }

    public void setCache(Cache cache) {
        this.cache = cache;
    }

    public CacheKeyTO getCacheKeyTO() {
        return cacheKeyTO;
    }

    public void setCacheKeyTO(CacheKeyTO cacheKeyTO) {
        this.cacheKeyTO = cacheKeyTO;
    }

    public long getLastLoadTime() {
        return lastLoadTime;
    }

    public void setLastLoadTime(long lastLoadTime) {
        this.lastLoadTime = lastLoadTime;
    }

    public long getLastRequestTime() {

        return lastRequestTime;
    }

    public void setLastRequestTime(long lastRequestTime) {
        synchronized (this){

            this.lastRequestTime = lastRequestTime;
            if (firstRequestTime == 0){
                firstRequestTime = lastRequestTime;
            }
            requestTimes++;
        }
    }

    public long getFirstRequestTime() {
        return firstRequestTime;
    }

    public void setFirstRequestTime(long firstRequestTime) {
        this.firstRequestTime = firstRequestTime;
    }

    public long getRequestTimes() {
        return requestTimes;
    }

    public void setRequestTimes(long requestTimes) {
        this.requestTimes = requestTimes;
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public String getCacheClass() {
        return cacheClass;
    }

    public void setCacheClass(String cacheClass) {
        this.cacheClass = cacheClass;
    }

    public String getCacheMethod() {
        return cacheMethod;
    }

    public void setCacheMethod(String cacheMethod) {
        this.cacheMethod = cacheMethod;
    }

    public long getLoadCnt() {
        return loadCnt;
    }

    public void setLoadCnt(long loadCnt) {
        this.loadCnt = loadCnt;
    }

    public long getUseTotalTime() {
        return useTotalTime;
    }

    public void setUseTotalTime(long useTotalTime) {
        this.useTotalTime = useTotalTime;
    }
}
