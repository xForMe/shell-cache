package com.crazy.shell.cache.to;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * 缓存key
 * Created by creazier.huang on 16/5/2.
 */
public class CacheKeyTO implements Serializable {
    /**
     * m命名空间
     */
    private String namespace;
    /**
     * 缓存key
     */
    private String key;
    /**
     * 设置哈希表中的字段，如果设置此项，则用哈希表进行存储
     */
    private String hfield;

    public CacheKeyTO() {
    }

    public CacheKeyTO(String namespace, String key) {
        this.namespace = namespace;
        this.key = key;
    }

    public String getCacheKey(){
        if(StringUtils.isNotEmpty(namespace)){
            return this.namespace+":"+this.key;
        }
        return this.key;
    }

    public String getFullKey(){
        StringBuilder b = new StringBuilder();
        if(StringUtils.isNotEmpty(namespace)){
            b.append(namespace).append(":");
        }
        b.append(key);
        if(StringUtils.isNotEmpty(hfield)){
            b.append(":").append(hfield);
        }
        return b.toString();
    }
    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getHfield() {
        return hfield;
    }

    public void setHfield(String hfield) {
        this.hfield = hfield;
    }
}
