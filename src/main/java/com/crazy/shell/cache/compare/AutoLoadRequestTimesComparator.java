package com.crazy.shell.cache.compare;

import com.crazy.shell.cache.to.AutoLoadTO;

import java.util.Comparator;

/**
 * shell-cache
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/3.
 */
public class AutoLoadRequestTimesComparator implements Comparator<AutoLoadTO> {
    @Override
    public int compare(AutoLoadTO o1, AutoLoadTO o2) {
        if(o1==null && o2 != null){
            return 1;
        }else if(o1!=null && o2 == null){
            return -1;
        }else if(o1==null && o2 == null){
            return 0;
        }


        return o1.getRequestTimes() > o2.getRequestTimes() ? -1 : 1;
    }
}
