package com.crazy.shell.cache.compare;

import com.crazy.shell.cache.to.AutoLoadTO;

import java.util.Comparator;

/**
 * shell-cache
 * 排序算法：越接近过期时间，越耗时的排在最前,即： System.currentTimeMillis() - autoLoadTO.getLastLoadTime()-autoLoadTO.getExpire()*1000 值越大，排在越前
 * autoLoadTO.getAverageUseTime() 值越大，排在越前
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/2.
 */
public class AutoLoadOldestComparator implements Comparator<AutoLoadTO> {


    @Override
    public int compare(AutoLoadTO o1, AutoLoadTO o2) {
        if(o1==null && o2 != null){
            return 1;
        }else if(o1!=null && o2 == null){
            return -1;
        }else if(o1==null && o2 == null){
            return 0;
        }
        long now=System.currentTimeMillis();
        long dif1=now - o1.getLastLoadTime() - o1.getCache().expire() * 1000;
        long dif2=now - o2.getLastLoadTime() - o2.getCache().expire() * 1000;
        if(dif1 > dif2) {
            return -1;
        } else if(dif1 < dif2) {
            return 1;
        } else {
            if(o1.getAverageUseTime() > o2.getAverageUseTime()) {
                return -1;
            } else if(o1.getAverageUseTime() < o2.getAverageUseTime()) {
                return 1;
            }
        }
        return 0;
    }
}
