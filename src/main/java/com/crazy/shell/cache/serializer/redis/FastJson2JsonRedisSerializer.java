package com.crazy.shell.cache.serializer.redis;

import com.alibaba.fastjson.JSON;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/6.
 */
public class FastJson2JsonRedisSerializer implements RedisSerializer {


    public byte[] serialize(Object o) throws SerializationException {

        return (o==null ? null : JSON.toJSONBytes(o));
    }

    public Object deserialize(byte[] bytes) throws SerializationException {
        return (bytes ==null ? null : JSON.parse(bytes));
    }
}
