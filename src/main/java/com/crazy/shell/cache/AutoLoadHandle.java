package com.crazy.shell.cache;

import com.crazy.shell.cache.annotation.Cache;
import com.crazy.shell.cache.to.AutoLoadConfig;
import com.crazy.shell.cache.to.AutoLoadTO;
import com.crazy.shell.cache.to.CacheWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * shell-cache
 * 创建人 : creazier.huang
 * 创建时间 : 16/5/9.
 */
public class AutoLoadHandle {

    private Logger logger = LoggerFactory.getLogger(AutoLoadHandle.class);

    /**
     * 自动加载队列
     */
    private ConcurrentHashMap<String,AutoLoadTO> autoLoadMap;

    /**
     * cacheManager
     */
    private ICacheManager cacheManager;

    /**
     * 缓存池
     */
    private Thread[] threads;

    /**
     * 排序进行,对自动加载队列进行排序
     */
    private Thread sortThread;

    /**
     * 自动加载队列
     */
    private LinkedBlockingQueue<AutoLoadTO> autoLoadQueue;

    private volatile boolean running = false;

    private AutoLoadConfig config;

    public AutoLoadHandle(ICacheManager cacheManager, AutoLoadConfig config) {
        this.cacheManager = cacheManager;
        this.config = config;
        this.running=true;
        this.threads = new Thread[this.config.getThreadCnt()];
        this.autoLoadMap = new ConcurrentHashMap<String, AutoLoadTO>(this.config.getMaxElement());
        this.autoLoadQueue = new LinkedBlockingQueue<AutoLoadTO>(this.config.getMaxElement());
        for(int i=0 ; i < this.config.getThreadCnt() ;i ++){
//            this.threads[i] = new Thread(new  )
        }
    }

    public AutoLoadTO[] getAutoLoadQueue(){
        if(autoLoadMap.isEmpty()){
            return null;
        }
        AutoLoadTO[] tmpArr = new AutoLoadTO[autoLoadMap.size()];
        tmpArr = autoLoadMap.values().toArray(tmpArr);
        if(null != config.getSortType() && null!= config.getSortType().getComparator()){
            Arrays.sort(tmpArr,config.getSortType().getComparator());
        }
        return tmpArr;

    }


    class SortRunnable implements Runnable{

        public void run() {
            while (running){
                if(autoLoadMap.isEmpty() || !autoLoadQueue.isEmpty()){  //没有数据或者主线程运行
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        logger.info("context",e);
                        Thread.currentThread().interrupt();
                    }
                    continue;
                }

                AutoLoadTO[] tmpArr = getAutoLoadQueue();
                if(null == tmpArr){
                    continue;
                }

                for(AutoLoadTO to : tmpArr){
                    try {
                        autoLoadQueue.put(to);
                    } catch (InterruptedException e) {
                        logger.info("context",e);
                    }
                }

            }
        }
    }

    class  AutoLoadRunnable implements Runnable{

        public void run() {
            while (running){
                try {
                    AutoLoadTO tmpTO = autoLoadQueue.take();
                    if(null != tmpTO){
                        loadCache(tmpTO);
                        Thread.sleep(config.getAutoLoadPeriod());
                    }
                } catch (InterruptedException e) {
                    logger.info("context",e);
                }
            }
        }
    }

    private void loadCache(AutoLoadTO autoLoadTO) {
        if(autoLoadTO  == null)
            return;
        long now = System.currentTimeMillis();
        if(autoLoadTO.getLastRequestTime() <= 0 || autoLoadTO.getLastLoadTime()<=0){
            return;
        }

        Cache cache = autoLoadTO.getCache();
        long requestTimeOut = cache.requestTimeout();
        int expire = cache.expire();
        if (requestTimeOut>0 && (now - autoLoadTO.getLastRequestTime()) >= requestTimeOut * 1000){ // 如果超出一定时间没有请求数据则从队列中删除
            autoLoadMap.remove(autoLoadTO.getCacheKeyTO().getFullKey());
            return;
        }
        if(autoLoadTO.isLoading()){
            return;
        }
        long timeout;
        if(expire >= 600){
            timeout = (expire-120) * 1000;
        }else {
            timeout = (expire-60) * 1000;
        }

        if(now - autoLoadTO.getLastLoadTime() >= timeout){
            if(config.isCheckFromCacheBeforeLoad()){
                CacheWrapper result = cacheManager.getCache(autoLoadTO.getCacheKeyTO());
                if (null != result && result.getLastLoadTime() > autoLoadTO.getLastLoadTime() && (now - result.getLastLoadTime()) < timeout) {// 如果已经被别的服务器更新了，则不需要再次更新
                    autoLoadTO.setLastLoadTime(result.getLastLoadTime());
                    return;
                }
            }
        }
//        cacheManager.loadData()
    }
}














